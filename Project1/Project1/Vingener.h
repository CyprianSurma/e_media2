#pragma once
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
class Vingener
{
public:
	bool Autokey;
	std::vector<std::vector<unsigned char>> AlphabetMatrix;
	std::vector<unsigned char>KeyMatrix;
	std::fstream Input, Output;
public:
	Vingener() :Autokey(false),AlphabetMatrix(1){};
	bool LoadAphabetFile(const char*)noexcept;
    bool LoadKeyFile(const char*)noexcept;
	bool OpenInputFile(const char* )noexcept;
	bool OpenOutputFile(const char*)noexcept;
	void Code()noexcept;
	void Decode()noexcept;
	~Vingener();
private:
	unsigned char CodeChar(const unsigned char, const unsigned char)noexcept;
	unsigned char DecodeChar(const unsigned char, const unsigned char)noexcept;

};

