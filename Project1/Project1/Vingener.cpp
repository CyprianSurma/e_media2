#include "Vingener.h"





Vingener::~Vingener()
{
}
bool Vingener::OpenInputFile(const char*filename)noexcept
{
	Input.open(filename);
	return Input.is_open();
}
bool Vingener::OpenOutputFile(const char*filename)noexcept
{
	Output.open(filename);
	return Input.is_open();
}
bool Vingener::LoadAphabetFile(const char*filename)noexcept
{
	int in, help;
	std::fstream file(filename);
	if (!file.is_open())
	{
		std::cout << "Nie mozna otworzyc:" << filename << "\n";
		return false;
	}

	while (file >> in)
	{
		if (in < 0 || in>256)
		{
			std::cout << "bledne wartosci alfabetu\n";
			return false;
		}

		AlphabetMatrix[0].push_back(in);
	}
	if (AlphabetMatrix[0].size() == 0)
	{
		std::cout << "pusty plik z alfabetem\n";
		return false;
	}
	auto end = std::unique(AlphabetMatrix[0].begin(), AlphabetMatrix[0].end());
	if (end != AlphabetMatrix[0].end())
	{
		std::cout << "Alfabet posiada powtarzajace sie znaki\n ";
		return false;
	}
	
	for (auto i = 1; i < AlphabetMatrix[0].size(); i++)
	{
		AlphabetMatrix.push_back({});
	}
	for (uint16_t i = 1; i < AlphabetMatrix[0].size(); ++i)
	{
		for (int j = 0; j < AlphabetMatrix[0].size(); ++j)
		{

			AlphabetMatrix[i].push_back(AlphabetMatrix[0][(j + i) % AlphabetMatrix[0].size()]);
		}
	}
	return true;
	



}
bool Vingener::LoadKeyFile(const char*filename)noexcept
{
	unsigned char input;
	std::fstream file(filename);
	if (!file.good())
	{
		std::cout << "Nie mozna otworzyc: " << filename << "\n";
		return false;
	}
	while (file >> input)
	{		
			if (std::find(AlphabetMatrix[0].begin(), AlphabetMatrix[0].end(), input) != AlphabetMatrix[0].end())
			{
				KeyMatrix.push_back(input);
			}
			else
			{
				std::cout << "Niepoprawny znak w kluczu : " << input << "\n";
				return false;
			}		
	}
	if (KeyMatrix.size() == 0)
	{
		std::cout << "Brak klucza\n";
		return false;
	}
	if (KeyMatrix.size() == 1)
	{
		Autokey = true;
	}
	return true;
}
void Vingener::Code()noexcept
{
	
	unsigned char in,last;
	int i = 0;
	if (Autokey)
	{
		last = KeyMatrix[0];
	}
	while (in=Input.get())
	{
		if (in ==0xFF)
		{
			break;
		}
		if (Autokey)
		{
			if (std::find(AlphabetMatrix[0].begin(), AlphabetMatrix[0].end(), in) != AlphabetMatrix[0].end())
			{
				Output << CodeChar(in, last);
				last = in;
			}
			else
				Output << in;
		}
		else
		{
			if (std::find(AlphabetMatrix[0].begin(), AlphabetMatrix[0].end(), in) != AlphabetMatrix[0].end())
			{
				if (i == KeyMatrix.size())
				{
					i = 0;
				}

				Output << CodeChar(in, KeyMatrix[i]);

				i++;
			}
			else
			{
				Output << in;
			}
		}
	}
}
void Vingener::Decode()noexcept
{

	unsigned char in, last;
	int i = 0;
	if (Autokey)
	{
		last = KeyMatrix[0];
	}
	while (in = Input.get())
	{
		if (in == 0xFF)
		{
			break;
		}
		if (Autokey)
		{
			if (std::find(AlphabetMatrix[0].begin(), AlphabetMatrix[0].end(), in) != AlphabetMatrix[0].end())
			{
				Output << DecodeChar(in, last);
				last = DecodeChar(in, last);
			}
			else
				Output << in;
		}
		else
		{
			if (std::find(AlphabetMatrix[0].begin(), AlphabetMatrix[0].end(), in) != AlphabetMatrix[0].end())
			{
				if (i == KeyMatrix.size())
				{
					i = 0;
				}

				Output << DecodeChar(in, KeyMatrix[i]);

				i++;
			}
			else
			{
				Output << in;
			}
		}
	}
}
unsigned char Vingener::CodeChar(const unsigned char chartocode,const unsigned char key)noexcept
{
	int i, j;
	bool found = false;
	for (i = 0; i < AlphabetMatrix[0].size(); ++i)
	{
		if (AlphabetMatrix[0][i] == chartocode)
		{
			found = true;
			break;
		}
	}
	for (j = 0; j < AlphabetMatrix[0].size(); ++j)
	{
		if (AlphabetMatrix[0][j] == key)
		{			
			break;
		}
	}
	if (found)
	{
		return AlphabetMatrix[i][j];
	}
	else
	{
		return chartocode;
	}
}
unsigned char Vingener::DecodeChar(const unsigned char chartocode, const unsigned char key)noexcept
{
	{
		int i, j;
		bool found = false;
		for (i = 0; i < AlphabetMatrix[0].size(); ++i)
		{
			if (AlphabetMatrix[0][i] == key)
			{
				found = true;
				break;
			}
		}		
		for (j = 0; j < AlphabetMatrix[0].size(); ++j)
		{
			if (AlphabetMatrix[j][i] == chartocode)
			{
				break;
			}
		}
		if (found)
		{
			return AlphabetMatrix[j][0];
		}
		else
		{
			return chartocode;
		}
	}
}